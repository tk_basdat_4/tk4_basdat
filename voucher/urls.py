from django.urls import path
from . import views

urlpatterns = [
    path('add/', views.add, name='add'),
    path('result/', views.result, name='laporan'),
    path('add/get-update-data/', views.getUpdateData, name='getupdatedata'),
    path('add/post-voucher/',views.voucheradd, name="voucheradd"),
    path('result/get-voucher/', views.getAllVoucher, name='getAllVoucher'),
    path('result/claim-event/', views.claimEvent, name="claimEvent"),
    path('result/delete-event/', views.deleteEvent, name='deleteEvent'),
    path('result/sort-asc/', views.sortAsc, name='sortAsc'),
	path('result/sort-desc/', views.sortDesc, name='sortDesc'),

]
