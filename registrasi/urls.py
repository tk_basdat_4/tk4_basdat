from django.urls import path, include
from . import views

urlpatterns = [
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('signup/',views.pickrole, name='pickrole'),
    path('signup/anggota/',views.anggota, name='anggota'),
    path('signup/petugas/',views.petugas, name='petugas'),
    path('post-login/',views.preLogin,name="preLogin"),
    path('post-signup/anggota/',views.preAnggota, name="preAnggota"),
    path('post-signup/petugas/',views.prePetugas, name="prePetugas"),
]
