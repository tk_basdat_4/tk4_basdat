from django.shortcuts import render
from django.db import connection
from django.contrib import messages
from django.http import HttpResponseRedirect
# Create your views here.
def add(request):
    return render (request, 'addStation.html')

def list(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    response = {}
    cursor.execute("select * from stasiun;")
    shasil = cursor.fetchall()
    response['stasiun'] = shasil
    print(response)
    return render (request, 'stationList.html', response)