$(document).ready(function(){
  $('#loader').delay("slow").hide();
  getAllAssignment();
  $("#filterAssignment").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#dataAssignment tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

var getAllAssignment = function(e) {
  console.log("jalanin fungsi");
  $.ajax ({
      url: "get-assignment/",
      dataType: "JSON",
      beforeSend: function() {
        $("#loader").css("display", "block");
      },
      success : function (response) {
          var data = "";
          var number = 1;
          for (var i = 0; i < response.length; i++) {
              console.log("sukses berfungsi");
              console.log(response[i].ktp_petugas)
              if (response[i] != undefined) {
                  data += 
                    "<tr>" +
                    "<td scope='row'>" + number + "</td>" +
                    "<td scope='row'>" + response[i].ktp_petugas + " - " + response[i].nama_petugas + "</td>" +
                    "<td scope='row'>" + response[i].tgl_mulai + "</td>" +
                    "<td scope='row'>" + response[i].tgl_akhir+ "</td>" +
                    "<td scope='row'>" + response[i].id_stasiun + " - " + response[i].nama_stasiun + "</td>" +                    
                    "<td scope='row'>" + "<button class='btn btn-primary' onclick='updateAssignment("+ response[i].ktp_petugas + " , " + response[i].tgl_mulai + " , " + response[i].id_stasiun +")'>Update</button></td>" +
                    "<td scope='row'>" + "<button class='btn btn-danger' onclick='deleteAssignment("+ response[i].ktp_petugas + " , " + response[i].tgl_mulai + " , " + response[i].id_stasiun +")'>Delete</button></td>" +
                    "</tr>";
              }
              number += 1 
          }
          $("#dataAssignment").html(data);
          $("#loader").css("display", "none");
      }
  })
}

$('#sortingAssignment').on('change', function() {
  if ($("#sortingAssignment").val() == "ascending") {
      $.ajax ({
          url: "sort-asc/",
          dataType: "JSON",
          beforeSend: function() {
            $("#dataAssignment").empty();
            $("#loader").css("display", "block");
          },
          success : function (response) {
              var data = "";
              var number = 1;
              for (var i = 0; i < response.length; i++) {
                  console.log("sukses berfungsi ascending");
                  console.log(response[i].ktp_petugas)
                  if (response[i] != undefined) {
                      data += 
                        "<tr>" +
                        "<td scope='row'>" + number + "</td>" +
                        "<td scope='row'>" + response[i].ktp_petugas + " - " + response[i].nama_petugas + "</td>" +
                        "<td scope='row'>" + response[i].tgl_mulai + "</td>" +
                        "<td scope='row'>" + response[i].tgl_akhir+ "</td>" +
                        "<td scope='row'>" + response[i].id_stasiun + " - " + response[i].nama_stasiun + "</td>" +                    
                        "<td scope='row'>" + "<button class='btn btn-primary' onclick='updateAssignment("+ response[i].ktp_petugas + " , " + response[i].tgl_mulai + " , " + response[i].id_stasiun +")'>Update</button></td>" +
                        "<td scope='row'>" + "<button class='btn btn-danger' onclick='deleteAssignment("+ response[i].ktp_petugas + " , " + response[i].tgl_mulai + " , " + response[i].id_stasiun +")'>Delete</button></td>" +
                        "</tr>";
                  }
                  number += 1 
              }
              $("#dataAssignment").html(data);
              $("#loader").css("display", "none");
          }
      })
  } else {
      $.ajax ({
          url: "sort-desc/",
          dataType: "JSON",
          beforeSend: function() {
            $("#dataAssignment").empty();
            $("#loader").css("display", "block");
          },
          success : function (response) {
              var data = "";
              var number = 1;
              for (var i = 0; i < response.length; i++) {
                  console.log("sukses berfungsi descending");
                  console.log(response[i].ktp_petugas)
                  if (response[i] != undefined) {
                      data += 
                        "<tr>" +
                        "<td scope='row'>" + number + "</td>" +
                        "<td scope='row'>" + response[i].ktp_petugas + " - " + response[i].nama_petugas + "</td>" +
                        "<td scope='row'>" + response[i].tgl_mulai + "</td>" +
                        "<td scope='row'>" + response[i].tgl_akhir+ "</td>" +
                        "<td scope='row'>" + response[i].id_stasiun + " - " + response[i].nama_stasiun + "</td>" +                    
                        "<td scope='row'>" + "<button class='btn btn-primary' onclick='updateAssignment("+ response[i].ktp_petugas + " , " + response[i].tgl_mulai + " , " + response[i].id_stasiun +")'>Update</button></td>" +
                        "<td scope='row'>" + "<button class='btn btn-danger' onclick='deleteAssignment("+ response[i].ktp_petugas + " , " + response[i].tgl_mulai + " , " + response[i].id_stasiun +")'>Delete</button></td>" +
                        "</tr>";
                  }
                  number += 1 
              }
              $("#dataAssignment").html(data);
              $("#loader").css("display", "none");
          }
      })   
  }
});   

var updateAssignment = function (ktp_petugas, tgl_mulai, id_stasiun) {
  console.log("update-assignment " + ktp_petugas);
  $.ajax ({
    type: 'GET',
    url: "/penugasan/add?ktp_petugas=" + ktp_petugas + "&tgl_mulai=" + tgl_mulai + "&id_stasiun=" + id_stasiun,
    data: {
        'ktp_petugas' : ktp_petugas,
        'tgl_mulai' : tgl_mulai,
        'id_stasiun': id_stasiun,
    },
    success: function(response){
        console.log("assignment updated!")
        window.location.href= "/penugasan/add?ktp_petugas=" + ktp_petugas + "&tgl_mulai=" + tgl_mulai + "&id_stasiun=" + id_stasiun;
    }
    
})
}


var deleteAssignment = function (ktp_petugas, tgl_mulai, id_stasiun) {
  var getConfirm = confirm("Are you sure want to delete this assignment ?");
  if( getConfirm == true ) {
      console.log("delete-assignment " + tgl_mulai + " & " + id_stasiun);
      $.ajax ({
          type: 'POST',
          url: 'delete-assignment/',
          data: {
              'ktp': ktp_petugas,
              'tgl_mulai': tgl_mulai,
              'id_stasiun': id_stasiun,
              csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
          },
          success: function(response){
              console.log("Deleted assignment by id " + tgl_mulai + " & " + id_stasiun);
              getAllAssignment();
              alert(response.messages)
          }
      })
      return true;
  } else {
      return false;
  }
};


// $('.selectpicker').selectpicker({
// 	  noneSelectedText : 'Pilih Stasiun (Bisa multiselect)',
//     style: 'btn-info',
//     size: 4
// });
