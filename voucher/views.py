from django.shortcuts import render
from django.http import JsonResponse
from django.db import connection
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from random import randint
import json

# Create your views here.
def add(request):
	return render (request, 'addVoucher.html')

def result(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("select * from voucher")
    currhasil = cursor.fetchall()
    response = {}
    response['vouchers'] = result
    return render (request, 'resultVoucher.html')

@csrf_exempt
def voucheradd(request):
	id_voucher = request.POST['id_voucher']
	nama= request.POST['nama']
	kategori= request.POST['kategori']
	poin = request.POST['poin']
	deskripsi = request.POST['deskripsi']
	jumlah = request.POST['jumlah']

	if id_voucher is None: 
		cursor = connection.cursor()
		cursor.execute('set search_path to public')
		cursor.execute("SELECT COUNT(*) FROM voucher")
		count_voucher = cursor.fetchone()[0]

		for i in jumlah:
			id_voucher = randint(count_voucher+1, 100)
			values = "'"+idVoucher+"','"+nama+"','"+kategori+"','"+poin+"','"+deskripsi+"'"
			query = "INSERT INTO voucher (id_voucher,nama,kategori,nilai_poin,deskripsi) values (" + values + ");"
			cursor.execute(query)

		context = {'messages': "Voucher berhasil ditambahkan!"}

	else: 
		id_voucher = id_voucher
		cursor = connection.cursor()
		cursor.execute('set search_path to public')
		cursor.execute("UPDATE voucher SET nama = %s, kategori = %s, nilai_poin = %s, deskripsi = %s WHERE id_voucher = %s", [nama,kategori,poin,deskripsi])
		context = {'messages': "Voucher telah diperbaharui!"}
	return JsonResponse(context)

def getAllVoucher(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * from Voucher")
    result = cursor.fetchall() 
    context = []
    for i in result:
    	context.append({"id_voucher" : i[0], "nama" : i[1], "kategori" : i[2],
    					"nilai_poin" : i[3], "deskripsi" : i[4]})
    return JsonResponse(context, safe = False)

def getUpdateData(request):
	id_voucher = request.GET['id_voucher']
	cursor = connection.cursor()
	cursor.execute('set search_path to public')
	cursor.execute("SELECT * FROM voucher WHERE id_voucher =" + "'" + id_voucher + "'")
	result = cursor.fetchall() 
	context = {}
	for res in result:
		context = {"id_voucher" : res[0], "nama" : res[1], "kategori" : res[2],
					"nilai_poin" : res[3], "deskripsi" : res[4]}
	return JsonResponse(context)

@csrf_exempt
def claimEvent(request):
	ktp = request.session['ktp']
	id_voucher = request.POST['id']
	with connection.cursor() as cursor:
		cursor.execute("UPDATE voucher SET no_kartu_anggota = '"+ ktp +"' where id_voucher ='" + id_voucher + "' ")
	context = {
		'messages': "Voucher telah berhasil diklaim!"
	}
	return JsonResponse(context)

@csrf_exempt
def deleteEvent(request):
	id_voucher = request.POST['id']
	with connection.cursor() as cursor:
		cursor.execute("DELETE FROM voucher WHERE id_voucher = " + "'" + id_voucher + "'")
	context = {
		'messages': "Voucher berhasil dihapus!"
	}
	return JsonResponse(context)

def sortAsc(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * from voucher ORDER BY nama ASC")
    result = cursor.fetchall() 
    context = []
    for i in result:
    	context.append({"id_voucher" : i[0], "nama" : i[1], "kategori" : i[2],
    					"nilai_poin" : i[3], "deskripsi" : i[4]})
    return JsonResponse(context, safe = False)

def sortDesc(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * from voucher ORDER BY nama DESC")
    result = cursor.fetchall() 
    context = []
    for i in result:
    	context.append({"id_voucher" : i[0], "nama" : i[1], "kategori" : i[2],
    					"nilai_poin" : i[3], "deskripsi" : i[4]})
    return JsonResponse(context, safe = False)