from django.urls import path, include
from . import views

urlpatterns = [
    path('add/', views.add, name='add'),
    path('result/', views.result, name='laporan'),
	path('add/get-data-station/', views.getDataStation, name='getdatastation'),
	path('add/get-update-data/', views.getUpdateData, name='getupdatedata'),
	path('add/new-event/', views.newEvent, name='newevent'),
	path('result/get-event/', views.getAllEvent, name='getallevent'),
	path('result/sort-asc/', views.sortAscending, name='sortascending'),
	path('result/sort-desc/', views.sortDescending, name='sortdescending'),
	path('result/delete-event/', views.deleteEvent, name='deleteevent'),
]
