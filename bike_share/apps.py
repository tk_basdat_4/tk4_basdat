from django.apps import AppConfig


class BikeShareConfig(AppConfig):
    name = 'bike_share'
