from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.db import connection
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.
def add(request):
	return render (request, 'addAssignment.html')

def result(request):
	return render (request, 'resultAssignment.html')

def getDataPetugas(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT nama, ktp FROM PETUGAS NATURAL JOIN PERSON")
    result = cursor.fetchall() 
    context = []
    for res in result:
    	context.append({"nama": res[0], "ktp": res[1]})
    return JsonResponse(context, safe = False)

def getDataStation(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT nama, id_stasiun FROM STASIUN")
    result = cursor.fetchall() 
    context = []
    for res in result:
    	context.append({"nama": res[0], "id_stasiun": res[1]})
    return JsonResponse(context, safe = False)

@csrf_exempt
def newAssignment(request):
	ktp = request.POST['petugas']
	tanggalmulai = request.POST['tanggalmulai']
	tanggalselesai = request.POST['tanggalselesai']
	id_stasiun = request.POST['stasiun']
	print(tanggalmulai)
	print(tanggalselesai)
	with connection.cursor() as cursor:
		cursor.execute("INSERT INTO PENUGASAN (ktp, start_datetime, id_stasiun, end_datetime) VALUES (%s, %s, %s, %s)", [ktp, tanggalmulai, id_stasiun, tanggalselesai])
	return render(request, 'resultAssignment.html')

def getAllAssignment(request):
	cursor = connection.cursor()
	cursor.execute('set search_path to public')
	cursor.execute("SELECT PT.ktp AS ktp_petugas, PR.nama AS nama_petugas, start_datetime::date, end_datetime::date, PN.id_stasiun, S.nama as nama_stasiun FROM PENUGASAN PN, PETUGAS PT, PERSON PR, STASIUN S WHERE PN.ktp = PT.ktp AND PT.ktp = PR.ktp AND PN.id_stasiun = S.id_stasiun")
	result = cursor.fetchall() 
	context = []
	for res in result:
		json_data = {
			"ktp_petugas" : res[0], 
			"nama_petugas" : res[1], 
			"tgl_mulai" : res[2],
			"tgl_akhir" : res[3], 
			"id_stasiun" : res[4],
			"nama_stasiun" : res[5],
		}
		context.append(json_data)
	return JsonResponse(context, safe = False)

def sortAscending(request):
	cursor = connection.cursor()
	cursor.execute('set search_path to public')
	cursor.execute("SELECT PT.ktp AS ktp_petugas, PR.nama AS nama_petugas, start_datetime::date, end_datetime::date, PN.id_stasiun, S.nama as nama_stasiun FROM PENUGASAN PN, PETUGAS PT, PERSON PR, STASIUN S WHERE PN.ktp = PT.ktp AND PT.ktp = PR.ktp AND PN.id_stasiun = S.id_stasiun ORDER BY ktp_petugas, nama_petugas, start_datetime::date ASC")
	result = cursor.fetchall() 
	context = []
	for res in result:
		json_data = {
			"ktp_petugas" : res[0], 
			"nama_petugas" : res[1], 
			"tgl_mulai" : res[2],
			"tgl_akhir" : res[3], 
			"id_stasiun" : res[4],
			"nama_stasiun" : res[5],
		}
		context.append(json_data)
	return JsonResponse(context, safe = False)

def sortDescending(request):
	cursor = connection.cursor()
	cursor.execute('set search_path to public')
	cursor.execute("SELECT PT.ktp AS ktp_petugas, PR.nama AS nama_petugas, start_datetime::date, end_datetime::date, PN.id_stasiun, S.nama as nama_stasiun FROM PENUGASAN PN, PETUGAS PT, PERSON PR, STASIUN S WHERE PN.ktp = PT.ktp AND PT.ktp = PR.ktp AND PN.id_stasiun = S.id_stasiun ORDER BY ktp_petugas, nama_petugas, start_datetime::date DESC")
	result = cursor.fetchall() 
	context = []
	for res in result:
		json_data = {
			"ktp_petugas" : res[0], 
			"nama_petugas" : res[1], 
			"tgl_mulai" : res[2],
			"tgl_akhir" : res[3], 
			"id_stasiun" : res[4],
			"nama_stasiun" : res[5],
		}
		context.append(json_data)
	return JsonResponse(context, safe = False)

def updateAssignment(request):
	ktp = request.GET['ktp_petugas']
	tgl_mulai = request.GET['tgl_mulai']
	id_stasiun = request.GET['id_stasiun']
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM PENUGASAN")
		result = cursor.fetchall() 
		print(result)
		for res in result:
			json_data = {
				"ktp_petugas" : res[0], 
				"nama_petugas" : res[1], 
				"tgl_mulai" : res[2],
				"tgl_akhir" : res[3], 
				"id_stasiun" : res[4],
				"nama_stasiun" : res[5],
			}
	return JsonResponse(json_data)

@csrf_exempt
def deleteAssignment(request):
	ktp = request.POST['ktp']
	tgl_mulai = request.POST['tgl_mulai']
	id_stasiun = request.POST['id_stasiun']
	print("delete by "+ ktp + " & " + id_stasiun)
	with connection.cursor() as cursor:
		cursor.execute("DELETE FROM PENUGASAN WHERE ktp = '" + ktp + "'" + " AND tgl_mulai = '" + tgl_mulai + "'" + " AND id_stasiun = '" + id_stasiun + "'")
	context = {
		'messages': "Assignment has been deleted!"
	}
	return JsonResponse(context)