$(document).ready(function(){
  getDataPetugas()
  getDataStation();
});

var getDataPetugas = function(e) {
  console.log("Load data petugas")
  $.ajax({
      url : "get-data-petugas/",
      dataType:"JSON",
      success : function (response) {
          var petugas   = '';
          for (var i = 0; i < response.length; i++) {
              petugas += "<option value='" + response[i].ktp + "'>" + response[i].nama + "</option>" ;
          };
          $("#inputPetugas").append(petugas);
          console.log("Data petugas available!")
      }
  });
}

var getDataStation = function(e) {
  console.log("Load data station")
  $.ajax({
      url : "get-data-station/",
      dataType:"JSON",
      success : function (response) {
          var station = '';
          for (var i = 0; i < response.length; i++) {
              station += "<option value='" + response[i].id_stasiun + "'>" + response[i].nama + "</option>" ;
          };
          $("#inputStasiun").append(station);
          console.log("Data stasiun available!")

      }
  });
}

$(function (e) {
    $("#formAssignment").on('submit', function(event) {
        event.preventDefault();
        console.log("form submitted");
        var petugas = $('#inputPetugas').val();
        var tanggalmulai = $('#inputTanggalMulai').val();
        var tanggalselesai = $('#inputTanggalSelesai').val();
        var stasiun = $('#inputStasiun').val();

        $.ajax({
            type : "POST",
            url : "new-assignment/",
            data : {
              'petugas': petugas,
              'tanggalmulai': tanggalmulai,
              'tanggalselesai': tanggalselesai,
              'stasiun': stasiun,
              csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            },
            success: function(response) {
                console.log("New assignment has been added!");
                alert("New assignment has been added!");
                $("#inputPetugas").val("");
                $("#inputTanggalMulai").val("");
                $("#inputTanggalSelesai").val("");
                $("#inputStasiun").val("");
                window.location.href= "/penugasan/result"
            },
            error: function (e) {
                console.log("error");
            }
        });
    });

});