from django.urls import reverse
from django.contrib import messages
from django.http import HttpResponse, JsonResponse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
import json
from django.shortcuts import render
from django.http import HttpResponseRedirect

def index (request):
   return render (request, 'home.html')

@csrf_exempt
def getSaldo(request):
   response = {}
   if(request.method == "POST"):
      cursor = connection.cursor()
      cursor.execute('set search_path to public')
      ktp = request.session['ktp']
      cursor.execute("select saldo from anggota where ktp ='" + ktp + "' ")
      saldo = cursor.fetchall()
      for row in saldo:
         saldo = row[0]
      response["currSaldo"] = saldo
      return JsonResponse(response)
   else:
      return HttpResponse(json.dumps({'message': " Try again "}),content_type="application/json")
