$(document).ready(function(){
  getAllEvent();
  $("#filterEvent").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#dataEvent tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

var getAllEvent = function(e) {
  console.log("jalanin fungsi");
  $.ajax ({
      url: "get-event/",
      dataType: "JSON",
      beforeSend: function() {
        $("#loader").css("display", "block");
      },
      success : function (response) {
          var data = "";
          var number = 1;
          for (var i = 0; i < response.length; i++) {
              console.log("sukses berfungsi");
              console.log(response);
              if (response[i] != undefined) {
                  data += 
                    "<tr>" +
                    "<td scope='row'>" + number + "</td>" +
                    "<td scope='row'>" + response[i].judul + "</td>" +
                    "<td scope='row'>" + response[i].deskripsi + "</td>" +
                    "<td scope='row'>" + response[i].tgl_mulai + "</td>" +
                    "<td scope='row'>" + response[i].tgl_akhir+ "</td>" +
                    "<td scope='row'>" + response[i].is_free+ "</td>" +
                    "<td scope='row'>" + "<button class='btn btn-primary' onclick='updateEvent("+ response[i].id_acara +")'>Update</button></td>" +
                    "<td scope='row'>" + "<button class='btn btn-danger' onclick='deleteEvent("+ response[i].id_acara +")'>Delete</button></td>" +
                    "</tr>";
              }
              number += 1
          }
          $("#dataEvent").html(data);
          $("#loader").css("display", "none");
      }
  })
}

$('#sortingEvent').on('change', function() {
  if ($("#sortingEvent").val() == "ascending") {
      $.ajax({
          url : "sort-asc/",
          dataType:"JSON",
          beforeSend: function() {
            $("#dataEvent").empty();
            $("#loader").css("display", "block");
          },
          success : function (response) {
              var data = "";
              var number = 1;
              for (var i = 0; i < response.length; i++) {
                  console.log("sukses berfungsi");
                  console.log(response);
                  if (response[i] != undefined) {
                      data += 
                        "<tr>" +
                        "<td scope='row'>" + number + "</td>" +
                        "<td scope='row'>" + response[i].judul + "</td>" +
                        "<td scope='row'>" + response[i].deskripsi + "</td>" +
                        "<td scope='row'>" + response[i].tgl_mulai + "</td>" +
                        "<td scope='row'>" + response[i].tgl_akhir+ "</td>" +
                        "<td scope='row'>" + response[i].is_free+ "</td>" +
                        "<td scope='row'>" + "<button class='btn btn-primary' onclick='updateEvent("+ response[i].id_acara +")'>Update</button></td>" +
                        "<td scope='row'>" + "<button class='btn btn-danger' onclick='deleteEvent("+ response[i].id_acara +")'>Delete</button></td>" +
                        "</tr>";
                  }
                  number += 1
              }
              $("#dataEvent").html(data);
              $("#loader").css("display", "none");
          }
      });
  } else {
      $.ajax({
          url : "sort-desc/",
          dataType:"JSON",
          beforeSend: function() {
            $("#dataEvent").empty();
            $("#loader").css("display", "block");
          },
          success : function (response) {
              var data = "";
              var number = 1;
              for (var i = 0; i < response.length; i++) {
                  console.log("sukses berfungsi");
                  console.log(response);
                  if (response[i] != undefined) {
                      data += 
                        "<tr>" +
                        "<td scope='row'>" + number + "</td>" +
                        "<td scope='row'>" + response[i].judul + "</td>" +
                        "<td scope='row'>" + response[i].deskripsi + "</td>" +
                        "<td scope='row'>" + response[i].tgl_mulai + "</td>" +
                        "<td scope='row'>" + response[i].tgl_akhir+ "</td>" +
                        "<td scope='row'>" + response[i].is_free+ "</td>" +
                        "<td scope='row'>" + "<button class='btn btn-primary' onclick='updateEvent("+ response[i].id_acara +")'>Update</button></td>" +
                        "<td scope='row'>" + "<button class='btn btn-danger' onclick='deleteEvent("+ response[i].id_acara +")'>Delete</button></td>" +
                        "</tr>";
                  }
                  number += 1
              }
              $("#dataEvent").html(data);
              $("#loader").css("display", "none");
          }
      });    
  }
});   

var updateEvent = function (id) {
  console.log("update-event " + id)
    $.ajax ({
      type: 'GET',
      url: "/acara/add?id_acara=" + id,
      data: {
          id: id,
      },
      success: function(response){
          console.log("event updated!")
          window.location.href="/acara/add?id_acara=" + id;
      }
      
  })
}

var deleteEvent = function (id) {
  var getConfirm = confirm("Are you sure want to delete this event ?");
  if( getConfirm == true ) {
      console.log("delete-event " + id );
      $.ajax ({
          type: 'POST',
          url: 'delete-event/',
          data: {
              id: id,
              csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
          },
          success: function(response){
              console.log("Deleted event by id "+ id);
              getAllEvent();
              alert(response.messages)
          }
      })
      return true;
  } else {
      return false;
  }
};


// $('.selectpicker').selectpicker({
// 	  noneSelectedText : 'Pilih Stasiun (Bisa multiselect)',
//     style: 'btn-info',
//     size: 4
// });
