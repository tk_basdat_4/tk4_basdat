from django.shortcuts import render
from django.db import connection
from django.contrib import messages
from django.http import HttpResponseRedirect

# Create your views here.
def add(request):
    return render (request, 'addBike.html')

def list(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    response={}
    cursor.execute("select se.*, st.nama from sepeda se, stasiun st where st.id_stasiun = se.id_stasiun;")
    bhasil = cursor.fetchall()
    response['sepeda'] = bhasil
    print(response)
    return render (request, 'bikeList.html', response)