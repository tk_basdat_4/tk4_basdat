$(document).ready(function(){
  // Get data all station
  var params = window.location.search;
  //params: ?id_acara=1
  console.log("params: " + params);

  var getURLParams = function(params) {
      var id_param = params.slice(params.indexOf('=') + 1);
      console.log("id_param: " + id_param);
      $.ajax({
        url : "get-update-data/",
        dataType: "JSON",
        data: {
          'id_acara': id_param,
        },
        success : function (response) {
          console.log(response)
          console.log(response.judul)
          console.log($('#inputJudul').val())
          $('#inputJudul').val(response.judul);
          $('#inputDeskripsi').val(response.deskripsi);
          $('#inputTanggalMulai').val(response.tgl_mulai);
          $('#inputTanggalSelesai').val(response.tgl_akhir);
          $('#inputGratis').val(response.is_free);
          // $('#inputStasiun').val(response.);
        }

    });    
  }
  params = getURLParams(params);
  getDataStation();

});

var getDataStation = function(e) {
  $.ajax({
      url : "get-data-station/",
      dataType:"JSON",
      success : function (response) {
          console.log(response);
          var station = '';
          for (var i = 0; i < response.length; i++) {
              station += "<option value='" + response[i].id_stasiun + "'>" + response[i].nama + "</option>" ;
          };
          console.log(station);
          $("#inputStasiun").html(station);
      }
  });
}

$(function (e) {
    $("#formEvent").on('submit', function(event) {
        event.preventDefault();
        console.log("form submitted");  
        var judul = $('#inputJudul').val();
        var deskripsi = $('#inputDeskripsi').val();
        var tgl_mulai = $('#inputTanggalMulai').val();
        var tgl_akhir = $('#inputTanggalSelesai').val();
        var is_free = $('#inputGratis').val();
        var stasiun = $('#inputStasiun').val();
        var params = window.location.search;
        var id_acara = params.slice(params.indexOf('=') + 1);
        console.log(id_acara)

        console.log(stasiun)

        $.ajax({
            type : "POST",
            url : "new-event/",
            data : {
              'id_acara': id_acara,
              'judul': judul,
              'deskripsi': deskripsi,
              'tgl_mulai': tgl_mulai,
              'tgl_akhir': tgl_akhir,
              'is_free': is_free,
              'stasiun': stasiun,
              csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            },
            success: function(response) {
                alert(response.messages)
                $("#inputTanggalMulai").val("");
                $("#inputTanggalSelesai").val("");
                $("#inputStasiun").val("");
                window.location.href= "/acara/result"
            },
            error: function (e) {
                console.log("error");
            }
        });
    });

});
