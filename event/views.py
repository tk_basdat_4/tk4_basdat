from django.shortcuts import render
from django.http import JsonResponse
from django.db import connection
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from random import randint
import json


# Create your views here.
def add(request):
	return render (request, 'addEvent.html')

def result(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * from ACARA")
    result = cursor.fetchall() 
    response = {}
    response['event'] = result
    return render (request, 'resultEvent.html')

def getDataStation(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT nama, id_stasiun FROM STASIUN")
    result = cursor.fetchall() 
    context = []
    for res in result:
    	context.append({"nama": res[0], "id_stasiun": res[1]})
    return JsonResponse(context, safe = False)

@csrf_exempt
def newEvent(request):
    id_acara = request.POST['id_acara']
    judul = request.POST['judul']
    deskripsi = request.POST['deskripsi']
    tgl_mulai = request.POST['tgl_mulai']
    tgl_akhir = request.POST['tgl_akhir']
    is_free_str = request.POST['is_free']
    id_stasiun = request.POST.getlist('stasiun') # NOT YET
    print(id_stasiun) #TO-DO

    if is_free_str == "Iya":
    	is_free = True
    else:
    	is_free = False

    if id_acara is None: 
        cursor = connection.cursor()
        cursor.execute('set search_path to public')
        cursor.execute("SELECT COUNT(*) FROM ACARA")
        count_acara = cursor.fetchone()[0]
        id_acara = randint(count_acara+1, 100)
        cursor.execute("INSERT INTO ACARA (id_acara, judul, deskripsi, tgl_mulai, tgl_akhir, is_free) VALUES (%s, %s, %s, %s, %s, %s)", [id_acara, judul, deskripsi, tgl_mulai, tgl_akhir, is_free])
        context = {
            'messages': "Event has been added!"
        }
    else: 
        id_acara = id_acara
        cursor = connection.cursor()
        cursor.execute('set search_path to public')
        cursor.execute("UPDATE ACARA SET judul = %s, deskripsi = %s, tgl_mulai = %s, tgl_akhir = %s, is_free = %s WHERE id_acara = %s", [judul, deskripsi, tgl_mulai, tgl_akhir, is_free, id_acara])
        context = {
            'messages': "Event has been updated!"
        }
    return JsonResponse(context)

	# BELUM DI ADD ACARA_STASIUN


def getAllEvent(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * from ACARA")
    result = cursor.fetchall() 
    context = []
    for res in result:
    	is_free = ''
    	if (res[5] == True):
    		is_free = "Ya"
    	else:
    		is_free = "Tidak"
    	context.append({"id_acara" : res[0], "judul" : res[1], "deskripsi" : res[2],
        				"tgl_mulai" : res[3], "tgl_akhir" : res[4], "is_free" : is_free})
    return JsonResponse(context, safe = False)


def sortAscending(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * from ACARA ORDER BY judul ASC")
    result = cursor.fetchall() 
    context = []
    for res in result:
    	is_free = ''
    	if (res[5] == True):
    		is_free = "Ya"
    	else:
    		is_free = "Tidak"
    	context.append({"id_acara" : res[0], "judul" : res[1], "deskripsi" : res[2],
        				"tgl_mulai" : res[3], "tgl_akhir" : res[4], "is_free" : is_free})
    return JsonResponse(context, safe = False)

def sortDescending(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * from ACARA ORDER BY judul DESC")
    result = cursor.fetchall() 
    context = []
    for res in result:
    	is_free = ''
    	if (res[5] == True):
    		is_free = "Ya"
    	else:
    		is_free = "Tidak"
    	context.append({"id_acara" : res[0], "judul" : res[1], "deskripsi" : res[2],
        				"tgl_mulai" : res[3], "tgl_akhir" : res[4], "is_free" : is_free})
    return JsonResponse(context, safe = False)

def getUpdateData(request):
    id_acara = request.GET['id_acara']
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * FROM ACARA WHERE id_acara =" + "'" + id_acara + "'")
    result = cursor.fetchall() 
    context = {}
    for res in result:
        is_free = ''
        if (res[5] == True):
            is_free = "Iya"
        else:
            is_free = "Tidak"
        context = {"id_acara" : res[0], "judul" : res[1], "deskripsi" : res[2],
                        "tgl_mulai" : res[3], "tgl_akhir" : res[4], "is_free" : is_free}
    return JsonResponse(context)

@csrf_exempt
def deleteEvent(request):
	id = request.POST['id']
	print("delete by "+ id)
	with connection.cursor() as cursor:
		cursor.execute("DELETE FROM ACARA WHERE id_acara = " + "'" + id + "'")
	context = {
		'messages': "Event has been deleted!"
	}
	return JsonResponse(context)