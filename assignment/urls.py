from django.urls import path
from . import views

urlpatterns = [
    path('add/', views.add, name='add'),
    path('result/', views.result, name='laporan'),
    path('add/get-data-petugas/', views.getDataPetugas, name='getdatapetugas'),
    path('add/get-data-station/', views.getDataStation, name='getdatastation'),
    path('add/new-assignment/', views.newAssignment, name='newassignment'),
	path('result/get-assignment/', views.getAllAssignment, name='getallassignment'),
	path('result/sort-asc/', views.sortAscending, name='sortascending'),
	path('result/sort-desc/', views.sortDescending, name='sortdescending'),
	path('result/update-assignment/', views.updateAssignment, name='updateassignment'),
	path('result/delete-assignment/', views.deleteAssignment, name='deleteassignment'),
]
