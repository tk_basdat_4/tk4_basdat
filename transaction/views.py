from django.shortcuts import render
from django.urls import reverse
from django.db import connection
from django.contrib import messages
from django.http import HttpResponseRedirect

def transaksi(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    response={}
    ktp = request.session['ktp']
    cursor.execute("select t.date_time,t.jenis,t.nominal from transaksi as t, anggota as a where a.ktp='" + ktp + "' and t.no_kartu_anggota = a.no_kartu;")
    currhasil = cursor.fetchall()
    response['transaksi'] = currhasil
    print(response)
    return render (request, 'transaksi.html',response)

def topupdef(request):  
    if(request.method == "POST"):
        saldo1 = request.POST['topup']    
        ktp = request.session['ktp']
        cursor = connection.cursor()
        cursor.execute('set search_path to public')
        cursor.execute("select no_kartu from anggota where ktp='" + ktp + "'") 
        nokartu=cursor.fetchone()
        nokartu=nokartu[0]
        cursor.execute("set timezone='asia/jakarta';")
        cursor.execute("insert into transaksi(no_kartu_anggota,date_time,jenis,nominal)values('"+str(nokartu)+"',now()::timestamp,'topup','"+saldo1+"')")
        messages.error(request, "Top Up Success")
        return HttpResponseRedirect('/transaksi/riwayat')    
    else:
        messages.error(request, "Transaction failed to attempt")
        return HttpResponseRedirect('/transaksi/topup')

def topup(request):
    return render (request, 'topup.html')

def rent(request):
    return render (request, 'bikeList.html')

def riwayatRent(request):
    return render (request, 'peminjaman.html')

def laporan(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    response={}
    cursor.execute("select n.nama, l.id_laporan,l.no_kartu_anggota, l.datetime_pinjam, l.status, p.denda from person as n, laporan as l,  anggota as a, peminjaman as p where n.ktp = a.ktp and a.no_kartu = l.no_kartu_anggota and l.no_kartu_anggota = p.no_kartu_anggota and l.datetime_pinjam= p.datetime_pinjam and l.nomor_sepeda = p.nomor_sepeda;")
    currhasil = cursor.fetchall()
    response['laporan'] = currhasil
    print(response)
    return render (request, 'laporan.html', response)

def historyRent(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to public')
    response={}
    
