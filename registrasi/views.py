from django.shortcuts import render
from django.db import connection
from django.urls import reverse
from django.contrib import messages
from random import randint
from django.http import HttpResponseRedirect


def login(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')

    return render(request,'login.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect('/')

def pickrole(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    else:
        return render(request,'pickrole.html')

def anggota(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    else:
        return render(request,'anggota.html')
    
def petugas(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    else:
        return render(request,'petugas.html')

def preLogin(request):
    currKtp = str(request.POST['ktp'])
    email = request.POST['email']
    ktp=''
    counter = 0
    for i in range(0,9):
        if(counter == 3):
            ktp+='-'
            ktp= ktp + currKtp[i]
            counter = 0
        else:
            ktp= ktp + currKtp[i]
        counter+=1

    cursor=connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * from person where ktp='"+ktp+"' and email='"+email+"'")
    result=cursor.fetchone();
    if (result):
        cursor.execute("SELECT * from petugas where ktp='"+ktp+"'")
        finalResult = cursor.fetchone();
        if(finalResult):
            cursor.execute("SELECT nama from person where ktp='"+ktp+"'")
            nama = cursor.fetchall()
            for row in nama:
                nama = row[0]
            request.session['nama'] = nama
            request.session['ktp'] = ktp
            request.session['email'] = email
            request.session['role'] = 'petugas'
            return HttpResponseRedirect('/penugasan/result')
        else:
            cursor.execute("SELECT * from anggota where ktp='"+ktp+"'")
            lastResult = cursor.fetchone()
            if(lastResult):
                cursor.execute("SELECT nama from person where ktp='"+ktp+"'")
                nama = cursor.fetchall()
                for row in nama:
                    nama = row[0]
                request.session['nama'] = nama
                request.session['ktp'] = ktp
                request.session['email'] = email
                request.session['role'] = 'anggota'
                return HttpResponseRedirect('/bikes/list')
            else:
                messages.error(request, "Your ID number or email is wrong")
                return HttpResponseRedirect('/registrasi/login')
    else:
        messages.error(request, "Your ID number or email is wrong")
        return HttpResponseRedirect('/registrasi/login')

def prePetugas(request):
    currID = str(request.POST['ktp'])
    if(len(currID) != 9):
        messages.error(request, "ID number must 9 digit")
        return HttpResponseRedirect('/registrasi/signup/petugas/')   
    else:
        nama= request.POST['nama']
        email= request.POST['email']
        hp = request.POST['hp']
        tanggal =request.POST['tanggal']
        alamat = request.POST['alamat']
        gaji='30000'
        ktp=''
        counter = 0
        for i in range(0,9):
            if(counter == 3):
                ktp+='-'
                ktp= ktp + currID[i]
                counter = 0
            else:
                ktp= ktp + currID[i]

            counter+=1
    
        cursor=connection.cursor()
        cursor.execute('set search_path to public')
        cursor.execute("SELECT * from person where email='"+email+"'")
        resultFinal=cursor.fetchone();
        if (resultFinal):
            if len(resultFinal) > 0:
                messages.error(request, "Email "+email+" has been registered")
                return HttpResponseRedirect('/registrasi/signup/petugas/')

        cursor=connection.cursor()
        cursor.execute('set search_path to public')
        cursor.execute("SELECT * from petugas where ktp='"+ktp+"'")
        result=cursor.fetchone();
        if (result):
            if len(result) > 0:
                messages.error(request, "ID Number "+ktp+" has been registered")
                return HttpResponseRedirect('/registrasi/signup/petugas/')
      
        values = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tanggal+"','"+hp+"'"
        query = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values (" + values + ")"
        cursor.execute(query)

        values = "'"+ktp+"',"+ gaji
        query = "INSERT INTO petugas (ktp,gaji) values (" + values + ")"
        cursor.execute(query)

        messages.success(request, "Registration Success: "+ nama)
        return HttpResponseRedirect('/registrasi/login/') 
   
def preAnggota(request):
    Ktpcurr = str(request.POST['ktp'])
    if(len(Ktpcurr) != 9):
        messages.error(request, "ID number must 9 digit")
        return HttpResponseRedirect('/registrasi/signup/anggota/')    
    else:
        nama= request.POST['nama']
        email= request.POST['email']
        hp = request.POST['hp']
        tanggal =request.POST['tanggal']
        alamat = request.POST['alamat']
        points = '0'
        saldo = '0'
        ktp=''
        counter = 0
        for i in range(0,9):
            if(counter == 3):
                ktp+='-'
                ktp= ktp + Ktpcurr[i]
                counter = 0
            else:
                ktp= ktp + Ktpcurr[i]
            counter+=1
        no_kartu = buat_kartu()

        cursor=connection.cursor()
        cursor.execute('set search_path to public')
        cursor.execute("SELECT * from person where email='"+email+"'")
        resultLast=cursor.fetchone();
        if (resultLast):
            if len(resultLast) > 0:
                messages.error(request, "Email "+email+" has been registered")
                return HttpResponseRedirect('/registrasi/signup/anggota/')
    
        cursor=connection.cursor()
        cursor.execute('set search_path to public')
        cursor.execute("SELECT * from anggota where ktp='"+ktp+"'")
        result=cursor.fetchone();
        if (result):
            if len(result) > 0:
                messages.error(request, "ID Number "+ktp+" has been registered")
                return HttpResponseRedirect('/registrasi/signup/anggota/')

        values = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tanggal+"','"+hp+"'"
        query = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values (" + values + ")"
        cursor.execute(query)

        values = "'"+no_kartu+"',"+ saldo + "," + points +"," + "'"+ktp+"'"
        query = "INSERT INTO anggota (no_kartu,saldo,points,ktp) values (" + values + ")"
        cursor.execute(query)

        messages.success(request, "Registration Success: "+ nama)
        return HttpResponseRedirect('/registrasi/login/')

def buat_kartu():
    kartu = randint(10000000, 99999999)
    kartu1 = str(kartu)
    no_kartu=''
    counter = 0
    for i in range(0,8):
        if(counter == 3):
            no_kartu+='-'
            no_kartu= no_kartu + kartu1[i]
            counter = 0
        else:
            no_kartu= no_kartu + kartu1[i]
        counter+=1

    cursor=connection.cursor()
    cursor.execute('set search_path to public')
    cursor.execute("SELECT * from anggota where no_kartu='"+no_kartu+"'")
    result=cursor.fetchone();
    if (result):
        if len(result) > 0:
            buat_kartu()    
    else:
        return no_kartu