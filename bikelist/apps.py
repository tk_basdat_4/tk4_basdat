from django.apps import AppConfig


class BikelistConfig(AppConfig):
    name = 'bikelist'
