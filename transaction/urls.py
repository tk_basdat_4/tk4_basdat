from django.urls import path
from . import views

urlpatterns = [
    path('riwayat/', views.transaksi, name='transaksi'),
    path('topup/', views.topup, name='topup'),
    path('laporan/', views.laporan, name='laporan'),
    path('post-topup/',views.topupdef, name="topupdef"),
    path('rent/', views.rent, name='rent'),
    path('riwayatRent/', views.riwayatRent, name='rent'),
    path('historyRent/', views.historyRent, name='historyRent'),
]
